"use strict";

"use strict";

import ClassicEditor from "@ckeditor/ckeditor5-editor-classic/src/classiceditor.js";

/*
    ! Import Orginal-CKeditor Features:
 */
import Alignment from "@ckeditor/ckeditor5-alignment/src/alignment";
import Autoformat from "@ckeditor/ckeditor5-autoformat/src/autoformat.js";
import AutoLink from '@ckeditor/ckeditor5-link/src/autolink';
import Base64UploadAdapter from "@ckeditor/ckeditor5-upload/src/adapters/base64uploadadapter";
import BlockQuote from "@ckeditor/ckeditor5-block-quote/src/blockquote.js";
import Bold from "@ckeditor/ckeditor5-basic-styles/src/bold.js";
import Essentials from "@ckeditor/ckeditor5-essentials/src/essentials.js";
import Font from "@ckeditor/ckeditor5-font/src/font";
import Highlight from "@ckeditor/ckeditor5-highlight/src/highlight";
import HorizontalLine from "@ckeditor/ckeditor5-horizontal-line/src/horizontalline";
import Image from "@ckeditor/ckeditor5-image/src/image.js";
import ImageCaption from "@ckeditor/ckeditor5-image/src/imagecaption.js";
import ImageResize from "@ckeditor/ckeditor5-image/src/imageresize";
import ImageStyle from "@ckeditor/ckeditor5-image/src/imagestyle.js";
import ImageToolbar from "@ckeditor/ckeditor5-image/src/imagetoolbar.js";
import ImageUpload from "@ckeditor/ckeditor5-image/src/imageupload.js";
import Indent from "@ckeditor/ckeditor5-indent/src/indent.js";
import IndentBlock from "@ckeditor/ckeditor5-indent/src/indentblock";
import Italic from "@ckeditor/ckeditor5-basic-styles/src/italic.js";
import Link from "@ckeditor/ckeditor5-link/src/link.js";
import LinkImage from "@ckeditor/ckeditor5-link/src/linkimage";
import List from "@ckeditor/ckeditor5-list/src/list.js";
import Paragraph from "@ckeditor/ckeditor5-paragraph/src/paragraph.js";
import PasteFromOffice from "@ckeditor/ckeditor5-paste-from-office/src/pastefromoffice";
import SpecialCharacters from "@ckeditor/ckeditor5-special-characters/src/specialcharacters";
import SpecialCharactersEssentials from "@ckeditor/ckeditor5-special-characters/src/specialcharactersessentials";
import Strikethrough from "@ckeditor/ckeditor5-basic-styles/src/strikethrough";
import Subscript from "@ckeditor/ckeditor5-basic-styles/src/subscript";
import Superscript from "@ckeditor/ckeditor5-basic-styles/src/superscript";
import Table from "@ckeditor/ckeditor5-table/src/table.js";
import TableCellProperties from "@ckeditor/ckeditor5-table/src/tablecellproperties";
import TableProperties from "@ckeditor/ckeditor5-table/src/tableproperties";
import TableToolbar from "@ckeditor/ckeditor5-table/src/tabletoolbar.js";
import TextTransformation from "@ckeditor/ckeditor5-typing/src/texttransformation.js";
import Underline from "@ckeditor/ckeditor5-basic-styles/src/underline";
/*
    *Import extern created Features:
 */
import Emoji from "@wwalc/ckeditor5-emoji/src/emoji";
/*
    *Import EicD created Features:
 */
import Heading from "@ckeditorGeoinfo/ckeditor5-heading-with-id";
/*
    *Import Custom CSS
 */
import "./custom.css";

class OctonuxArticleEditor extends ClassicEditor {
}

const customColors = [
    { color: "hsl(6, 54%, 95%)", label: " " },
    { color: "hsl(6, 54%, 89%)", label: " " },
    { color: "hsl(6, 54%, 78%)", label: " " },
    { color: "hsl(6, 54%, 68%)", label: " " },
    { color: "hsl(6, 54%, 57%)", label: " " },
    { color: "hsl(6, 63%, 46%)", label: " " },
    { color: "hsl(6, 63%, 41%)", label: " " },
    { color: "hsl(6, 63%, 35%)", label: " " },
    { color: "hsl(6, 63%, 29%)", label: " " },
    { color: "hsl(6, 63%, 24%)", label: " " },
    { color: "hsl(6, 78%, 96%)", label: " " },
    { color: "hsl(6, 78%, 91%)", label: " " },
    { color: "hsl(6, 78%, 83%)", label: " " },
    { color: "hsl(6, 78%, 74%)", label: " " },
    { color: "hsl(6, 78%, 66%)", label: " " },
    { color: "hsl(6, 78%, 57%)", label: " " },
    { color: "hsl(6, 59%, 50%)", label: " " },
    { color: "hsl(6, 59%, 43%)", label: " " },
    { color: "hsl(6, 59%, 37%)", label: " " },
    { color: "hsl(6, 59%, 30%)", label: " " },
    { color: "hsl(283, 39%, 95%)", label: " " },
    { color: "hsl(283, 39%, 91%)", label: " " },
    { color: "hsl(283, 39%, 81%)", label: " " },
    { color: "hsl(283, 39%, 72%)", label: " " },
    { color: "hsl(283, 39%, 63%)", label: " " },
    { color: "hsl(283, 39%, 53%)", label: " " },
    { color: "hsl(283, 34%, 47%)", label: " " },
    { color: "hsl(283, 34%, 40%)", label: " " },
    { color: "hsl(283, 34%, 34%)", label: " " },
    { color: "hsl(283, 34%, 28%)", label: " " },
    { color: "hsl(282, 39%, 95%)", label: " " },
    { color: "hsl(282, 39%, 89%)", label: " " },
    { color: "hsl(282, 39%, 79%)", label: " " },
    { color: "hsl(282, 39%, 68%)", label: " " },
    { color: "hsl(282, 39%, 58%)", label: " " },
    { color: "hsl(282, 44%, 47%)", label: " " },
    { color: "hsl(282, 44%, 42%)", label: " " },
    { color: "hsl(282, 44%, 36%)", label: " " },
    { color: "hsl(282, 44%, 30%)", label: " " },
    { color: "hsl(282, 44%, 25%)", label: " " },
    { color: "hsl(204, 51%, 94%)", label: " " },
    { color: "hsl(204, 51%, 89%)", label: " " },
    { color: "hsl(204, 51%, 78%)", label: " " },
    { color: "hsl(204, 51%, 67%)", label: " " },
    { color: "hsl(204, 51%, 55%)", label: " " },
    { color: "hsl(204, 64%, 44%)", label: " " },
    { color: "hsl(204, 64%, 39%)", label: " " },
    { color: "hsl(204, 64%, 34%)", label: " " },
    { color: "hsl(204, 64%, 28%)", label: " " },
    { color: "hsl(204, 64%, 23%)", label: " " },
    { color: "hsl(204, 70%, 95%)", label: " " },
    { color: "hsl(204, 70%, 91%)", label: " " },
    { color: "hsl(204, 70%, 81%)", label: " " },
    { color: "hsl(204, 70%, 72%)", label: " " },
    { color: "hsl(204, 70%, 63%)", label: " " },
    { color: "hsl(204, 70%, 53%)", label: " " },
    { color: "hsl(204, 62%, 47%)", label: " " },
    { color: "hsl(204, 62%, 40%)", label: " " },
    { color: "hsl(204, 62%, 34%)", label: " " },
    { color: "hsl(204, 62%, 28%)", label: " " },
    { color: "hsl(168, 55%, 94%)", label: " " },
    { color: "hsl(168, 55%, 88%)", label: " " },
    { color: "hsl(168, 55%, 77%)", label: " " },
    { color: "hsl(168, 55%, 65%)", label: " " },
    { color: "hsl(168, 55%, 54%)", label: " " },
    { color: "hsl(168, 76%, 42%)", label: " " },
    { color: "hsl(168, 76%, 37%)", label: " " },
    { color: "hsl(168, 76%, 32%)", label: " " },
    { color: "hsl(168, 76%, 27%)", label: " " },
    { color: "hsl(168, 76%, 22%)", label: " " },
    { color: "hsl(168, 42%, 94%)", label: " " },
    { color: "hsl(168, 42%, 87%)", label: " " },
    { color: "hsl(168, 42%, 74%)", label: " " },
    { color: "hsl(168, 42%, 61%)", label: " " },
    { color: "hsl(168, 45%, 49%)", label: " " },
    { color: "hsl(168, 76%, 36%)", label: " " },
    { color: "hsl(168, 76%, 31%)", label: " " },
    { color: "hsl(168, 76%, 27%)", label: " " },
    { color: "hsl(168, 76%, 23%)", label: " " },
    { color: "hsl(168, 76%, 19%)", label: " " },
    { color: "hsl(145, 45%, 94%)", label: " " },
    { color: "hsl(145, 45%, 88%)", label: " " },
    { color: "hsl(145, 45%, 77%)", label: " " },
    { color: "hsl(145, 45%, 65%)", label: " " },
    { color: "hsl(145, 45%, 53%)", label: " " },
    { color: "hsl(145, 63%, 42%)", label: " " },
    { color: "hsl(145, 63%, 37%)", label: " " },
    { color: "hsl(145, 63%, 32%)", label: " " },
    { color: "hsl(145, 63%, 27%)", label: " " },
    { color: "hsl(145, 63%, 22%)", label: " " },
    { color: "hsl(145, 61%, 95%)", label: " " },
    { color: "hsl(145, 61%, 90%)", label: " " },
    { color: "hsl(145, 61%, 80%)", label: " " },
    { color: "hsl(145, 61%, 69%)", label: " " },
    { color: "hsl(145, 61%, 59%)", label: " " },
    { color: "hsl(145, 63%, 49%)", label: " " },
    { color: "hsl(145, 63%, 43%)", label: " " },
    { color: "hsl(145, 63%, 37%)", label: " " },
    { color: "hsl(145, 63%, 31%)", label: " " },
    { color: "hsl(145, 63%, 25%)", label: " " },
    { color: "hsl(48, 89%, 95%)", label: " " },
    { color: "hsl(48, 89%, 90%)", label: " " },
    { color: "hsl(48, 89%, 80%)", label: " " },
    { color: "hsl(48, 89%, 70%)", label: " " },
    { color: "hsl(48, 89%, 60%)", label: " " },
    { color: "hsl(48, 89%, 50%)", label: " " },
    { color: "hsl(48, 88%, 44%)", label: " " },
    { color: "hsl(48, 88%, 38%)", label: " " },
    { color: "hsl(48, 88%, 32%)", label: " " },
    { color: "hsl(48, 88%, 26%)", label: " " },
    { color: "hsl(37, 90%, 95%)", label: " " },
    { color: "hsl(37, 90%, 90%)", label: " " },
    { color: "hsl(37, 90%, 80%)", label: " " },
    { color: "hsl(37, 90%, 71%)", label: " " },
    { color: "hsl(37, 90%, 61%)", label: " " },
    { color: "hsl(37, 90%, 51%)", label: " " },
    { color: "hsl(37, 86%, 45%)", label: " " },
    { color: "hsl(37, 86%, 39%)", label: " " },
    { color: "hsl(37, 86%, 33%)", label: " " },
    { color: "hsl(37, 86%, 27%)", label: " " },
    { color: "hsl(28, 80%, 95%)", label: " " },
    { color: "hsl(28, 80%, 90%)", label: " " },
    { color: "hsl(28, 80%, 81%)", label: " " },
    { color: "hsl(28, 80%, 71%)", label: " " },
    { color: "hsl(28, 80%, 61%)", label: " " },
    { color: "hsl(28, 80%, 52%)", label: " " },
    { color: "hsl(28, 74%, 46%)", label: " " },
    { color: "hsl(28, 74%, 39%)", label: " " },
    { color: "hsl(28, 74%, 33%)", label: " " },
    { color: "hsl(28, 74%, 27%)", label: " " },
    { color: "hsl(24, 71%, 94%)", label: " " },
    { color: "hsl(24, 71%, 88%)", label: " " },
    { color: "hsl(24, 71%, 77%)", label: " " },
    { color: "hsl(24, 71%, 65%)", label: " " },
    { color: "hsl(24, 71%, 53%)", label: " " },
    { color: "hsl(24, 100%, 41%)", label: " " },
    { color: "hsl(24, 100%, 36%)", label: " " },
    { color: "hsl(24, 100%, 31%)", label: " " },
    { color: "hsl(24, 100%, 26%)", label: " " },
    { color: "hsl(24, 100%, 22%)", label: " " },
    { color: "hsl(192, 15%, 99%)", label: " " },
    { color: "hsl(192, 15%, 99%)", label: " " },
    { color: "hsl(192, 15%, 97%)", label: " " },
    { color: "hsl(192, 15%, 96%)", label: " " },
    { color: "hsl(192, 15%, 95%)", label: " " },
    { color: "hsl(192, 15%, 94%)", label: " " },
    { color: "hsl(192, 5%, 82%)", label: " " },
    { color: "hsl(192, 3%, 71%)", label: " " },
    { color: "hsl(192, 2%, 60%)", label: " " },
    { color: "hsl(192, 1%, 49%)", label: " " },
    { color: "hsl(204, 8%, 98%)", label: " " },
    { color: "hsl(204, 8%, 95%)", label: " " },
    { color: "hsl(204, 8%, 90%)", label: " " },
    { color: "hsl(204, 8%, 86%)", label: " " },
    { color: "hsl(204, 8%, 81%)", label: " " },
    { color: "hsl(204, 8%, 76%)", label: " " },
    { color: "hsl(204, 5%, 67%)", label: " " },
    { color: "hsl(204, 4%, 58%)", label: " " },
    { color: "hsl(204, 3%, 49%)", label: " " },
    { color: "hsl(204, 3%, 40%)", label: " " },
    { color: "hsl(184, 9%, 96%)", label: " " },
    { color: "hsl(184, 9%, 92%)", label: " " },
    { color: "hsl(184, 9%, 85%)", label: " " },
    { color: "hsl(184, 9%, 77%)", label: " " },
    { color: "hsl(184, 9%, 69%)", label: " " },
    { color: "hsl(184, 9%, 62%)", label: " " },
    { color: "hsl(184, 6%, 54%)", label: " " },
    { color: "hsl(184, 5%, 47%)", label: " " },
    { color: "hsl(184, 5%, 40%)", label: " " },
    { color: "hsl(184, 5%, 32%)", label: " " },
    { color: "hsl(184, 6%, 95%)", label: " " },
    { color: "hsl(184, 6%, 91%)", label: " " },
    { color: "hsl(184, 6%, 81%)", label: " " },
    { color: "hsl(184, 6%, 72%)", label: " " },
    { color: "hsl(184, 6%, 62%)", label: " " },
    { color: "hsl(184, 6%, 53%)", label: " " },
    { color: "hsl(184, 5%, 46%)", label: " " },
    { color: "hsl(184, 5%, 40%)", label: " " },
    { color: "hsl(184, 5%, 34%)", label: " " },
    { color: "hsl(184, 5%, 27%)", label: " " },
    { color: "hsl(210, 12%, 93%)", label: " " },
    { color: "hsl(210, 12%, 86%)", label: " " },
    { color: "hsl(210, 12%, 71%)", label: " " },
    { color: "hsl(210, 12%, 57%)", label: " " },
    { color: "hsl(210, 15%, 43%)", label: " " },
    { color: "hsl(210, 29%, 29%)", label: " " },
    { color: "hsl(210, 29%, 25%)", label: " " },
    { color: "hsl(210, 29%, 22%)", label: " " },
    { color: "hsl(210, 29%, 18%)", label: " " },
    { color: "hsl(210, 29%, 15%)", label: " " },
    { color: "hsl(210, 9%, 92%)", label: " " },
    { color: "hsl(210, 9%, 85%)", label: " " },
    { color: "hsl(210, 9%, 70%)", label: " " },
    { color: "hsl(210, 9%, 55%)", label: " " },
    { color: "hsl(210, 14%, 39%)", label: " " },
    { color: "hsl(210, 29%, 24%)", label: " " },
    { color: "hsl(210, 29%, 21%)", label: " " },
    { color: "hsl(210, 29%, 18%)", label: " " },
    { color: "hsl(210, 29%, 16%)", label: " " },
    { color: "hsl(210, 29%, 13%)", label: " " }
];

/*
	Set specials extern SpecialCharacters!
 */
function SpecialCharactersEmoji(editor) {
    editor.plugins.get("SpecialCharacters").addItems("Emoji", [
        { title: "smiley face", character: "😊" },
        { title: "rocket", character: "🚀" },
        { title: "wind blowing face", character: "🌬️" },
        { title: "floppy disk", character: "💾" },
        { title: "heart", character: "❤️" }
    ]);
}

function SpecialCharactersArrowsExtended(editor) {
    editor.plugins.get("SpecialCharacters").addItems("Arrows", [
        { title: "simple arrow left", character: "←" },
        { title: "simple arrow up", character: "↑" },
        { title: "simple arrow right", character: "→" },
        { title: "simple arrow down", character: "↓" }
    ]);
}

// Plugins to include in the build.
OctonuxArticleEditor.builtinPlugins = [
    Alignment,
    Autoformat,
    AutoLink,
    Base64UploadAdapter,
    BlockQuote,
    Bold,
    Emoji,
    Essentials,
    Font,
    Heading,
    HorizontalLine,
    Highlight,
    Image,
    ImageCaption,
    LinkImage,
    ImageResize,
    ImageStyle,
    ImageToolbar,
    ImageUpload,
    Indent,
    IndentBlock,
    Italic,
    Link,
    List,
    Paragraph,
    PasteFromOffice,
    SpecialCharacters,
    SpecialCharactersArrowsExtended,
    SpecialCharactersEmoji,
    SpecialCharactersEssentials,
    Strikethrough,
    Subscript,
    Superscript,
    Table,
    TableCellProperties,
    TableProperties,
    TableToolbar,
    TextTransformation,
    Underline
];

ClassicEditor.defaultConfig = {
    toolbar: {
        viewportTopOffset: 50,
        items: [
            "undo",
            "redo",
            "|",
            "heading",
            "|",
            "bold",
            "italic",
            "strikethrough",
            "underline",
            "fontSize",
            "fontColor",
            "fontBackgroundColor",
            "highlight",
            "|",
            "indent",
            "outdent",
            "alignment",
            "|",
            "subscript",
            "superscript",
            "|",
            "link",
            "|",
            "bulletedList",
            "numberedList",
            "|",
            "blockQuote",
            "horizontalLine",
            "insertTable",
            "|",
            "imageUpload",
            "|",
            "specialCharacters",
            "emoji"
        ]
    },
    alignment: {
        options: ["left", "center", "justify", "right"]
    },
    emoji: [
        { name: "smile", text: "😀" },
        { name: "wink", text: "😉" },
        { name: "cool", text: "😎" },
        { name: "surprise", text: "😮" },
        { name: "confusion", text: "😕" },
        { name: "crying", text: "😢" },
        { name: "angry", text: "😠" },
        { name: "map", text: "🗺️" },
        { name: "needle", text: "📍" },
        { name: "world", text: "🌍" },
        { name: "internet", text: "🌐" },
        { name: "computer", text: "🖥️" },
        { name: "laptop", text: "💻" },
        { name: "100", text: "💯" }
    ],
    fontBackgroundColor: {
        columns: 10,
        documentColors: 10,
        colors: customColors
    },
    fontColor: {
        columns: 10,
        documentColors: 10,
        colors: customColors
    },
    fontFamily: {
        options: [
            "default"
        ]
    },
    fontSize: {
        options: [
            9,
            11,
            13,
            "default",
            17,
            19,
            21
        ]
    },
    heading: {
        options: [
            { model: "paragraph", title: "Paragraph", class: "ck-heading_paragraph" },
            { model: "heading1", view: "h1", title: "Heading 1", class: "ck-heading_heading1" },
            { model: "heading2", view: "h2", title: "Heading 2", class: "ck-heading_heading2" },
            { model: "heading3", view: "h3", title: "Heading 3", class: "ck-heading_heading3" },
            { model: "heading4", view: "h4", title: "Heading 4", class: "ck-heading_heading4" },
            { model: "heading5", view: "h5", title: "Heading 5", class: "ck-heading_heading5" },
            { model: "heading6", view: "h6", title: "Heading 6", class: "ck-heading_heading6" },
        ]
    },
    highlight: {
        options: [
            {
                model: "geoinfoMarker",
                class: "marker-pink",
                title: "GEOINFO marker",
                color: "var(--ck-highlight-marker-pink)",
                type: "marker"
            }
        ]
    },
    image: {
        toolbar: [
            "imageStyle:alignLeft",
            "imageStyle:alignCenter",
            "imageStyle:alignRight",
            "|",
            "imageResize",
            "|",
            "imageTextAlternative",
            "|",
            "linkImage"
        ],
        styles: [
            "full",
            "side",
            "alignLeft",
            "alignCenter",
            "alignRight"

        ],
        resizeOptions: [
            {
                name: "imageResize:original",
                value: null,
                label: "Original"
            },
            {
                name: "imageResize:1000",
                value: 1000,
                label: "1000"
            },
            {
                name: "imageResize:900",
                value: 900,
                label: "900"
            },
            {
                name: "imageResize:800",
                value: 800,
                label: "800"
            },
            {
                name: "imageResize:700",
                value: 700,
                label: "700"
            },
            {
                name: "imageResize:600",
                value: 600,
                label: "600"
            },
            {
                name: "imageResize:500",
                value: 500,
                label: "500"
            },
            {
                name: "imageResize:400",
                value: 400,
                label: "400"
            },
            {
                name: "imageResize:300",
                value: 300,
                label: "300"
            },
            {
                name: "imageResize:250",
                value: 250,
                label: "250"
            },
            {
                name: "imageResize:200",
                value: 200,
                label: "200"
            },
            {
                name: "imageResize:150",
                value: 150,
                label: "150"
            },
            {
                name: "imageResize:100",
                value: 100,
                label: "100"
            },
            {
                name: "imageResize:50",
                value: 50,
                label: "50"
            }

        ],
        resizeUnit: "px"
    },
    indentBlock: {
        offset: 1,
        unit: "em"
    },
    link: {
        addGreenLink: {
            mode: 'automatic',
            attributes: {
                class: 'my-green-link'
            }
        },
        decorators: {
            openInNewTab: {
                mode: 'manual',
                label: 'Open in a new tab',
                attributes: {
                    target: '_blank',
                    rel: 'noopener noreferrer'
                }
            }
        },
        defaultProtocol: "https://www."
    },
    table: {
        contentToolbar: [
            "tableColumn",
            "tableRow",
            "mergeTableCells",
            "tableProperties",
            "tableCellProperties"
        ],
        tableProperties: {},
        tableCellProperties: {}
    },

    // This value must be kept in sync with the language defined in webpack.config.js.
    language: "en"
};

export default OctonuxArticleEditor;
